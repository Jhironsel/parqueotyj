package Formularios;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;

public class frmEntrada extends javax.swing.JDialog implements Runnable {
    /**
     * Declaramos las variables que van a contener los resultados de salida
     */
    int hora, minutos, segundos, dia, mes, anno;
    Calendar calendario;
    Thread h1;

    public frmEntrada(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        h1 = new Thread(this);
        h1.start();
    }

    public static String getFechaActual() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
        return formateador.format(ahora);
    }

    @Override
    public void run() {
        Thread ct = Thread.currentThread();
        while (ct == h1) {
            calcula();
            jHora.setText(hora + ":" + minutos + ":" + segundos);
            jFecha.setText(getFechaActual());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }

    public void calcula() {
        Calendar calendario = new GregorianCalendar();
        hora = calendario.get(Calendar.HOUR_OF_DAY);
        minutos = calendario.get(Calendar.MINUTE);
        segundos = calendario.get(Calendar.SECOND);
        dia = calendario.get(Calendar.DATE);
        mes = calendario.get(Calendar.DAY_OF_WEEK_IN_MONTH);
        anno = calendario.get(Calendar.YEAR);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtPlaca = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jcbClientes = new javax.swing.JComboBox<>();
        jButton2 = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jFecha = new javax.swing.JLabel();
        jHora = new javax.swing.JLabel();
        jFecha1 = new javax.swing.JLabel();
        jFecha2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jlCantidadUsuario = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Entrada al Estacionamiento");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButton1.setText("Entrada");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/tom-y-jerry-logo.png"))); // NOI18N

        txtPlaca.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtPlaca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPlacaKeyReleased(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Placa #:");

        jcbClientes.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jButton2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButton2.setText("Cancelar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2), "Fecha", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Agency FB", 0, 24))); // NOI18N

        jFecha.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jFecha.setText("50");

        jHora.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jHora.setText("50");

        jFecha1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jFecha1.setText("Fecha de Ingreso");

        jFecha2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jFecha2.setText("Hora de Ingreso");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jHora)
                    .addComponent(jFecha)
                    .addComponent(jFecha1)
                    .addComponent(jFecha2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jFecha1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jFecha)
                .addGap(8, 8, 8)
                .addComponent(jFecha2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jHora)
                .addGap(25, 25, 25))
        );

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Cantidad de Usuario encontrados:");

        jlCantidadUsuario.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlCantidadUsuario.setText("1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 17, Short.MAX_VALUE)
                        .addComponent(jButton2)
                        .addGap(18, 18, 18)
                        .addComponent(jButton1))
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(8, 8, 8))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jlCantidadUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jcbClientes, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtPlaca))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(txtPlaca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jcbClientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jlCantidadUsuario))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1)
                            .addComponent(jButton2))
                        .addContainerGap())))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed
        String[] mivector = new String[10000];
    private void txtPlacaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPlacaKeyReleased
        //Haciendo validaciones con el Archivo Cliente.txt
        jcbClientes.removeAllItems();
        String lClientes;
        String lVehiculos;
        FileReader frClientes = null;
        FileReader frVehiculos = null;
        int i = 0;
        try {

            File clientes = new File("src/archivos/Clientes.txt");
            frClientes = new FileReader(clientes.getAbsolutePath());

            File vehiculos = new File("src/archivos/Vehiculos.txt");
            frVehiculos = new FileReader(vehiculos.getAbsolutePath());

            BufferedReader brClientes = new BufferedReader(frClientes);
            BufferedReader brVehiculos = new BufferedReader(frVehiculos);

            brClientes.readLine();
            brVehiculos.readLine();

            lClientes = brClientes.readLine();
            lVehiculos = brVehiculos.readLine();
            //Comparar cedula
            while (lClientes != null) {
                String vCliente[] = lClientes.split("<>");

                while (lVehiculos != null) {
                    String vVehiculo[] = lVehiculos.split("<>");
                    if (vCliente[0].equals(vVehiculo[0])) {
                        if (txtPlaca.getText().trim().equals("")) {
                            activar();
                            jlCantidadUsuario.setText(""+jcbClientes.getItemCount());
                            i = 0;
                            return;
                        }
                        if (vVehiculo[1].startsWith(txtPlaca.getText().trim())) {
                            jcbClientes.addItem(vCliente[1] + " " + vCliente[2]);
                            mivector[i] = vVehiculo[1];                            
                            i++;
                        }
                    }
                    jlCantidadUsuario.setText(""+jcbClientes.getItemCount());
                    lVehiculos = brVehiculos.readLine();
                }
                if (lVehiculos == null) {
                    vehiculos = new File("src/archivos/Vehiculos.txt");
                    frVehiculos = new FileReader(vehiculos.getAbsolutePath());
                    brVehiculos = new BufferedReader(frVehiculos);
                    lVehiculos = "";
                    brVehiculos.readLine();
                    lVehiculos = brVehiculos.readLine();
                }
                lClientes = brClientes.readLine();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(frmLogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(frmLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                frClientes.close();
                frVehiculos.close();
            } catch (IOException ex) {
                Logger.getLogger(frmLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        // Fin de validaciones...
    }//GEN-LAST:event_txtPlacaKeyReleased
    private void activar() {
        //Haciendo validaciones con el Archivo Cliente.txt
        jcbClientes.removeAllItems();
        FileReader fr = null;
        try {
            File fichero = new File("src/archivos/Clientes.txt");
            fr = new FileReader(fichero.getAbsolutePath());
            BufferedReader br = new BufferedReader(fr);
            String linea = "";
            br.readLine();
            linea = br.readLine();
            while (linea != null) {
                String vector[] = linea.split("<>");
                jcbClientes.addItem(vector[1] + " " + vector[2]);
                linea = br.readLine();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(frmLogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(frmLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fr.close();
            } catch (IOException ex) {
                Logger.getLogger(frmLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        jlCantidadUsuario.setText(""+jcbClientes.getItemCount());
        // Fin de validaciones...
    }
    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        activar();
    }//GEN-LAST:event_formWindowOpened
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if(jcbClientes.getItemCount() == 0){
            JOptionPane.showMessageDialog(this, "Debe Selecionar un Cliente");
            return;
        }
        if(jcbClientes.getSelectedIndex() == -1){
            JOptionPane.showMessageDialog(this, "Debe Selecionar un Cliente");
            return;
        }
        
        int IDEstacion = 0;
        //Haciendo validaciones con el Archivo Cliente.txt
        FileReader fr = null;
        try {
            File fichero = new File("src/archivos/VehiculosEstacionados.txt");
            fr = new FileReader(fichero.getAbsolutePath());
            BufferedReader br = new BufferedReader(fr);
            String linea = "";
            br.readLine();
            linea = br.readLine();
            while (linea != null) {
                String vector[] = linea.split("<>");
                if (vector[0] == null) {
                    //algo

                } else {
                    //algo
                    IDEstacion = Integer.parseInt(vector[0]) + 1;
                }
                linea = br.readLine();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(frmLogin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(frmLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fr.close();
            } catch (IOException ex) {
                Logger.getLogger(frmLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        // Fin de validaciones...

        //Pasamos a escribir al archivo Cliente....
        String Vehiculo = IDEstacion + "<>" + mivector[jcbClientes.getSelectedIndex()] + "<>"
                + jcbClientes.getSelectedItem().toString() + "<>"
                + jFecha.getText() + "<>" + jHora.getText() + "<>" + "SI";
        FileWriter fw = null;
        try {
            // TODO code application logic here (algoritmo que escribe archivo)
            File fichero = new File("src/archivos/VehiculosEstacionados.txt");
            fw = new FileWriter(fichero.getAbsolutePath(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);
            pw.println(Vehiculo);
            pw.close();
            JOptionPane.showMessageDialog(this, "Vehiculo Registrado con exito");
        } catch (IOException ex) {
            Logger.getLogger(frmRegistroCliente.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                Logger.getLogger(frmRegistroCliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jFecha;
    private javax.swing.JLabel jFecha1;
    private javax.swing.JLabel jFecha2;
    private javax.swing.JLabel jHora;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JComboBox<String> jcbClientes;
    private javax.swing.JLabel jlCantidadUsuario;
    private javax.swing.JTextField txtPlaca;
    // End of variables declaration//GEN-END:variables
}
